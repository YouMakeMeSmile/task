package io.velog.youmakemesmile.plugin;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class MyPlugin implements Plugin<Project> {

    @Override
    public void apply(Project project) {
//        project.getTasks().create("myTask", MyTask.class);
        project.getTasks().create("generateTypeScriptEnum", GenerateTypeScriptEnum.class);
//                .dependsOn(project.getTasks().getByName("build"));
//        project.getTasks().create("analysisEntitySpec", AnalysisEntitySpec.class).dependsOn(project.getTasks().getByName("build"));

    }
}