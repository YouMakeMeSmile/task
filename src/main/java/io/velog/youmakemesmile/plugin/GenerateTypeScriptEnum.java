package io.velog.youmakemesmile.plugin;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.EnumDeclaration;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

public class GenerateTypeScriptEnum extends DefaultTask {

    @TaskAction
    public void initDir() throws IOException {
        System.out.println("██████╗  ██████╗ ██████╗ ██████╗ ██╗   ██╗     ");
        System.out.println("██╔══██╗██╔═══██╗██╔══██╗██╔══██╗╚██╗ ██╔╝     ");
        System.out.println("██║  ██║██║   ██║██████╔╝██████╔╝ ╚████╔╝      ");
        System.out.println("██║  ██║██║   ██║██╔══██╗██╔══██╗  ╚██╔╝       ");
        System.out.println("██████╔╝╚██████╔╝██████╔╝██████╔╝   ██║        ");
        System.out.println("╚═════╝  ╚═════╝ ╚═════╝ ╚═════╝    ╚═╝        ");
        System.out.println("                                               ");
        System.out.println("██╗███████╗    ███████╗██████╗ ███████╗███████╗");
        System.out.println("██║██╔════╝    ██╔════╝██╔══██╗██╔════╝██╔════╝");
        System.out.println("██║███████╗    █████╗  ██████╔╝█████╗  █████╗  ");
        System.out.println("██║╚════██║    ██╔══╝  ██╔══██╗██╔══╝  ██╔══╝  ");
        System.out.println("██║███████║    ██║     ██║  ██║███████╗███████╗");
        System.out.println("╚═╝╚══════╝    ╚═╝     ╚═╝  ╚═╝╚══════╝╚══════╝");


        String generatedPath = getProject().getProjectDir().getPath() + "/.generated/";
        String srcPath = getProject().getProjectDir().getPath() + "/src/";

        if (Files.exists(Paths.get(generatedPath))) {
            Files.walk(Paths.get(generatedPath))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
        Files.walk(Paths.get(srcPath))
                .filter(Files::isRegularFile)
                .filter(value -> value.toString().endsWith("java"))
                .forEach(file -> {
                    try {
                        CompilationUnit c = StaticJavaParser.parse(file);
                        if (c.getTypes().size() > 0 && c.getType(0) instanceof EnumDeclaration && c.getPackageDeclaration().isPresent()) {
                            EnumDeclaration enumDeclaration = (EnumDeclaration) c.getType(0);
                            String packagee = c.getPackageDeclaration().get().getName().asString();
                            if ((packagee.startsWith("io.velog.youmakemesmile"))) {
                                StringBuilder enumSb = new StringBuilder();
                                enumSb.append(String.format("enum %s {\n", enumDeclaration.getNameAsString()));

                                StringBuilder labelSb = new StringBuilder();
                                labelSb.append(String.format("const %sLabel = new Map<%s, string>([\n", enumDeclaration.getNameAsString(), enumDeclaration.getNameAsString()));
                                enumDeclaration.getEntries()
                                        .forEach(value -> {
                                            enumSb.append(String.format("\t %s = '%s', // code:%s, value:%s\n", value.getNameAsString(), value.getNameAsString(), value.getArgument(0).toString(), value.getArgument(1).toString()));
                                            labelSb.append(String.format("\t[%s.%s, '%s'],\n", enumDeclaration.getNameAsString(), value.getNameAsString(), value.getArgument(1).toString().replace("\"", "")));
                                        });
                                enumSb.append("}\n");
                                enumSb.append("\n");
                                labelSb.append("]);\n");
                                labelSb.append("\n");

                                StringBuilder exportSb = new StringBuilder();
                                exportSb.append("export {\n");
                                exportSb.append(String.format("\t%s,\n", enumDeclaration.getNameAsString()));
                                exportSb.append(String.format("\t%sLabel,\n", enumDeclaration.getNameAsString()));
                                exportSb.append("};\n");
                                PackageDeclaration packageDeclaration = c.getPackageDeclaration().get();
                                c.getPackageDeclaration().get().getNameAsString().split(".");
                                Files.createDirectories(Paths.get(generatedPath + packageDeclaration.getName().getIdentifier()));
                                Files.writeString(Paths.get(generatedPath + packageDeclaration.getName().getIdentifier() + "/" + enumDeclaration.getNameAsString() + ".ts"), enumSb.append(labelSb).append(exportSb), StandardOpenOption.CREATE);
                            }

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

        File[] a = new File(generatedPath).listFiles();
        for (File folder : a) {
            StringBuilder indexSb = new StringBuilder();
            Arrays.stream(folder.listFiles()).forEach(file -> {
                indexSb.append(String.format("export * from './%s';\n", file.getName().replace(".ts", "")));
            });
            Files.writeString(Paths.get(generatedPath + folder.getName() + "/" + "index.ts"), indexSb, StandardOpenOption.CREATE);
        }
    }

    public static void main(String[] args) throws IOException {
        System.out.println("██████╗  ██████╗ ██████╗ ██████╗ ██╗   ██╗     ");
        System.out.println("██╔══██╗██╔═══██╗██╔══██╗██╔══██╗╚██╗ ██╔╝     ");
        System.out.println("██║  ██║██║   ██║██████╔╝██████╔╝ ╚████╔╝      ");
        System.out.println("██║  ██║██║   ██║██╔══██╗██╔══██╗  ╚██╔╝       ");
        System.out.println("██████╔╝╚██████╔╝██████╔╝██████╔╝   ██║        ");
        System.out.println("╚═════╝  ╚═════╝ ╚═════╝ ╚═════╝    ╚═╝        ");
        System.out.println("                                               ");
        System.out.println("██╗███████╗    ███████╗██████╗ ███████╗███████╗");
        System.out.println("██║██╔════╝    ██╔════╝██╔══██╗██╔════╝██╔════╝");
        System.out.println("██║███████╗    █████╗  ██████╔╝█████╗  █████╗  ");
        System.out.println("██║╚════██║    ██╔══╝  ██╔══██╗██╔══╝  ██╔══╝  ");
        System.out.println("██║███████║    ██║     ██║  ██║███████╗███████╗");
        System.out.println("╚═╝╚══════╝    ╚═╝     ╚═╝  ╚═╝╚══════╝╚══════╝");

//        String generatedPath = "C:\\Users\\nextree\\Desktop\\release\\ecom-prologue\\ecom-prologue\\.generated\\";
//        String srcPath = "C:\\Users\\nextree\\Desktop\\release\\ecom-prologue\\ecom-prologue\\src";
        String generatedPath = "/Users/kch/Downloads/test/.generated/";
        String srcPath = "/Users/kch/Downloads/test/src";


        if (Files.exists(Paths.get(generatedPath))) {
            Files.walk(Paths.get(generatedPath))
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
        Files.walk(Paths.get(srcPath))
                .filter(Files::isRegularFile)
                .filter(value -> value.toString().endsWith("java"))
                .forEach(file -> {
                    try {
                        CompilationUnit c = StaticJavaParser.parse(file);
                        if (c.getTypes().size() > 0 && c.getType(0) instanceof EnumDeclaration && c.getPackageDeclaration().isPresent()) {
                            EnumDeclaration enumDeclaration = (EnumDeclaration) c.getType(0);
                            String packagee = c.getPackageDeclaration().get().getName().asString();
                            if ((packagee.startsWith("io.velog.youmakemesmile"))) {
                                StringBuilder enumSb = new StringBuilder();
                                enumSb.append(String.format("enum %s {\n", enumDeclaration.getNameAsString()));

                                StringBuilder labelSb = new StringBuilder();
                                labelSb.append(String.format("const %sLabel = new Map<%s, string>([\n", enumDeclaration.getNameAsString(), enumDeclaration.getNameAsString()));
                                enumDeclaration.getEntries()
                                        .forEach(value -> {
                                            enumSb.append(String.format("\t %s = '%s', // code:%s, value:%s\n", value.getNameAsString(), value.getNameAsString(), value.getArgument(0).toString(), value.getArgument(1).toString()));
                                            labelSb.append(String.format("\t[%s.%s, '%s'],\n", enumDeclaration.getNameAsString(), value.getNameAsString(), value.getArgument(1).toString().replace("\"", "")));
                                        });
                                enumSb.append("}\n");
                                enumSb.append("\n");
                                labelSb.append("]);\n");
                                labelSb.append("\n");

                                StringBuilder exportSb = new StringBuilder();
                                exportSb.append("export {\n");
                                exportSb.append(String.format("\t%s,\n", enumDeclaration.getNameAsString()));
                                exportSb.append(String.format("\t%sLabel,\n", enumDeclaration.getNameAsString()));
                                exportSb.append("};\n");
                                PackageDeclaration packageDeclaration = c.getPackageDeclaration().get();
                                c.getPackageDeclaration().get().getNameAsString().split(".");
                                Files.createDirectories(Paths.get(generatedPath + packageDeclaration.getName().getIdentifier()));
                                Files.writeString(Paths.get(generatedPath + packageDeclaration.getName().getIdentifier() + "/" + enumDeclaration.getNameAsString() + ".ts"), enumSb.append(labelSb).append(exportSb), StandardOpenOption.CREATE);
                            }

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

        File[] a = new File(generatedPath).listFiles();
        for (File folder : a) {
            StringBuilder indexSb = new StringBuilder();
            Arrays.stream(folder.listFiles()).forEach(file -> {
                indexSb.append(String.format("export * from './%s';\n", file.getName().replace(".ts", "")));
            });
            Files.writeString(Paths.get(generatedPath + folder.getName() + "/" + "index.ts"), indexSb, StandardOpenOption.CREATE);
        }


    }
}
